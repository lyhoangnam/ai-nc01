import matplotlib.pyplot as plt
import tensorflow as tf

href = "https://developers.google.com/codelabs/tensorflow-2-computervision#0"
print("from", href)
print("tensorflow.version", tf.__version__)


labels = {0: "T-shirt/top", 1: "Trouser", 2: "Pullover", 3: "Dress",
          4: "Coat", 5: "Sandal", 6: "Shirt", 7: "Sneaker", 8: "Bag", 9: "Ankle Boot"}
mnist = tf.keras.datasets.fashion_mnist

(training_images, training_labels), (test_images, test_labels) = mnist.load_data()

plt.imshow(training_images[0])
# plt.show()
# plt.imshow(test_images[0])
# plt.show()

# chuẩn hóa image thành 0 với 1(image normalization)
training_images = training_images / 255.0
test_images = test_images / 255.0

# có 3 lớp mô hình
model = tf.keras.models.Sequential([tf.keras.layers.Flatten(),
                                    tf.keras.layers.Dense(
                                        128, activation=tf.nn.relu),
                                    tf.keras.layers.Dense(10, activation=tf.nn.softmax)])

model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss="sparse_categorical_crossentropy",
              metrics=["accuracy"])

# a higher epochs, a greater overfitting
model.fit(training_images, training_labels, epochs=5)

model.evaluate(test_images, test_labels)

classifications = model.predict(test_images)
plt.imshow(test_images[0])

print("classifications", classifications[0])
print("test_labels", labels[test_labels[0]])

print("hello stop exam.03")
