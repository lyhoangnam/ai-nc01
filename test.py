# import numpy as np
# import gzip
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier


def predict(data, labels, xyz):
    model = KNeighborsClassifier(n_neighbors=1)
    model.fit(data, labels)
    predicted = model.predict([xyz])
    return predicted


customers = [
    [1.73, 0.01, 5.22],
    [0.03, 4.41, 2.05],
    [1.13, 0.89, 3.76],
]

products = [
    [3.29, 3.44, 3.67],
    [0.82, 9.71, 3.88],
    [8.34, 1.72, 0.02],
]

labels = ["b", "g", "r"]

xyz = customers[1]

class_ = predict(data=products, labels=labels, xyz=xyz)
print("Dự đoán {xyz}".format(xyz=xyz), class_)


fig = plt.figure()
ax = plt.axes(projection='3d')


ax.scatter3D(xyz[0], xyz[1], xyz[2], c="m", cmap="Greens", marker="X")

ax.scatter3D(products[0][0], products[0][1],
             products[0][2], c="b", cmap="Greens")
ax.scatter3D(products[1][0], products[1][1],
             products[1][2], c="g", cmap="Greens")
ax.scatter3D(products[2][0], products[2][1],
             products[2][2], c="r", cmap="Greens")


# đề xuất sản phẩm số 1
plt.savefig("output/exam.02/chart/09.png")



# read idx3-ubyte data
# f = gzip.open("data/exam.03/test10k-images-idx3-ubyte.gz","r")
# image_size = 28
# num_images = 5
# f.read()
# buf = f.read(image_size * image_size * num_images)
# data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
# data = data.reshape(num_images, image_size, image_size, 1)
# img = np.asarray(data[2]).squeeze()
# plt.imshow(img)
# plt.show()