# Lời nói đầu
Trước tiên xin cảm ơn ủy ban nhân dân Thành Phố Hồ Chí Minh, các trường đại học, các thầy cô cùng các nhà tài trợ đã tổ chức các khóa học phổ cập kiến thức cho người dân Việt Nam. Đặc biệt cảm ơn các thầy cô đã trực tiếp giảng dạy trong đó có thầy Nguyễn Hải Đăng - giáo viên khóa NC01 - đã tích cực chỉ dẫn chúng em trong suốt quá trình học tập.

[Bài tập này][link_exam] được thực hiện bởi học viên mang mã số **AIC2021-SV104 - Lý Hoàng Nam**. Trong quá trình làm bài có tham khảo nhiều nguồn tài liệu khác nhau từ Google, Youtube, cũng như các trang chính thống của các packages scikit-learn, matplotlib, numpy...Bài tập được bắt đầu thực hiện vào mùng 1 Tết Âm Lịch năm Nhâm Dần 2022 và kết thúc vào tối ngày 2022-02-08.

Ứng dụng colab quả thật là một công cụ tuyệt vời hỗ trợ code, tuy nhiên bản thân là một lập trình viên, em muốn biết từng dòng lệnh của mình chạy được trên hệ điều hành nào, phiên bản python nào, các gói packages nào...nên em mạng phép không sử dụng colab mà viết một console application để hoàn tất bài tập của mình. Các file output sẽ xuất đầy đủ vào folder output bao gồm các file python, png và html.

# Cài đặt
Như có đề cập ở trên, tài liệu này chỉ hướng dẫn cài đặt trên hệ điều hành Window(window 10 trở lên hoặc window server 2012 trở lên). Yêu cầu là đã cài ***hệ điều hành window*** và ***python phiên bản 3.10.0***. Với các phiên bản hệ điều hành khác hoặc python phiên bản khác thì yêu cầu phải cài gói ***scikit-learn*** với phiên bản tương ứng.

1. Cài đặt hệ điều hành window
2. Cài đặt python phiên bản 3.10.0([link download python][dowload_python])
3. Trong quá trình cài đặt python thì cần add path vào window(chú ý có một dấu check)
4. Dowload source code tại đây ```git clone https://gitlab.com/lyhoangnam/ai-nc01.git -b exam```. Nếu chưa có git thì có thể vào [link này][gitlab_ainc01] để download source code dạng file zip về
5. Sau khi dowload thì có thể view code bằng bất cứ IDE nào(kiến nghị là dùng [Visual Studio Code][download_visual_studio_code])
6. Chạy ứng dụng bằng trình command line hoặc power shell hoặc cmder.
    - Trỏ vào thư mục **ai-nc01** gõ câu lệnh ```install```. Câu lệnh này gọi thực thi file *install.cmd*. Lưu ý nếu chưa có đầy đủ các thư viện thì quá trình cài đặt có thể mất 5'
    - Gõ câu lệnh ```run``` để thực thi việc chạy ra output các bài tập(lưu ý bài tập 1 chạy cả 10 thuật toán nên có thể mất 15')
    - Sau khi chạy xong câu lệnh trên thì kết quả được lưu lại trong folder **output**

# Bài tập 1
Tại bài tập này thì em đã làm đầy đủ 10 thuật toán như mẫu từ link [kaggle][kaggle_practical]. File data được sử dụng từ chính link gốc kaggle để dễ kiểm tra kết quả(***[/data/USA_Housing.csv][download_csv]***)
- File code là file ***[exam.01.py][exam_01_py]***
- Chỉ chạy bài tập 1 ```python exam.01.py```
- Output trong folder ***[/output/exam.01/...][output_exam_01]***
- Các bước tiến hành theo thứ tự bên dưới


### 1.1 Import dữ liệu đầu vào

<img alt="Import dữ liệu đầu vào" src="./output/exam.01/screenshot/screenshot.00.png" />

### 1.2 Phân tích dữ liệu(Exploratory Data Analysis - EDA)

<img alt="Phân tích dữ liệu" src="./output/exam.01/chart/01. seaborn.pairplot.png" />

<img alt="hvplot.hist.price.01" src="./output/exam.01/chart/02. hvplot.hist.price.01.png" />

<img alt="hvplot.hist.price.02" src="./output/exam.01/chart/03. hvplot.hist.price.02.png" />

<img alt="scatter.house.age.price" src="./output/exam.01/chart/04. scatter.house.age.price.png" />

<img alt="scatter.income.price" src="./output/exam.01/chart/05. scatter.income.price.png" />

<img alt="heatmap" src="./output/exam.01/chart/06. heatmap.png" />



### 1.3.1 Linear Regression

<img alt="screenshot Linear Regression" src="./output/exam.01/screenshot/screenshot.01.png" />

<img alt="predicted" src="./output/exam.01/chart/07. predicted.png" />

<img alt="Residual Histogram" src="./output/exam.01/chart/08. error.values.png" />


### 1.3.2 Robust Regression

<img alt="screenshot Robust Regression" src="./output/exam.01/screenshot/screenshot.02.png" />


### 1.3.3 Ridge Regression

<img alt="screenshot Ridge Regression" src="./output/exam.01/screenshot/screenshot.03.png" />


### 1.3.4 LASSO Regression

<img alt="screenshot LASSO Regression" src="./output/exam.01/screenshot/screenshot.04.png" />


### 1.3.5 Elastic Net

<img alt="screenshot Elastic Net" src="./output/exam.01/screenshot/screenshot.05.png" />

### 1.3.6 Polynomial Regression

<img alt="screenshot Polynomial Regression" src="./output/exam.01/screenshot/screenshot.06.png" />

### 1.3.7 Stochastic Gradient Descent

<img alt="screenshot Stochastic Gradient Descent" src="./output/exam.01/screenshot/screenshot.07.png" />

### 1.3.8 Artficial Neural Network

<img alt="screenshot Artficial Neural Network" src="./output/exam.01/screenshot/screenshot.08.01.png" />

<img alt="screenshot tensorflow.01" src="./output/exam.01/screenshot/screenshot.08.02.png" />

<img alt="screenshot tensorflow.08" src="./output/exam.01/screenshot/screenshot.08.08.png" />

<img alt="screenshot tensorflow.09" src="./output/exam.01/screenshot/screenshot.08.09.png" />

<img alt="scatter.tensorflow.model" src="./output/exam.01/chart/09. scatter.tensorflow.model.png" />

<img alt="scatter.tensorflow.history" src="./output/exam.01/chart/10. scatter.tensorflow.history.png" />


### 1.3.9 Random Forest Regressor

<img alt="screenshot Random Forest Regressor" src="./output/exam.01/screenshot/screenshot.09.png" />

### 1.3.10 Support Vector Machine

<img alt="screenshot Support Vector Machine" src="./output/exam.01/screenshot/screenshot.10.png" />

### 1.4 Models Comparison

<img alt="Models Comparison" src="./output/exam.01/chart/11. model.comparison.png" />

# Bài tập 2
- Khởi tạo dữ liệu theo yêu cầu của đề bài bao gồm 300 điểm tọa độ.
- Khởi tạo hàm ***kmeans***
    - Khởi tạo điểm trọng tâm ngẫu nhiên(centroids)
    - Tùy chỉnh lại điểm trọng tâm(new_centroids)
    - Cấu hình nhãn cho các nhóm điểm(labels)
    - Trả lại danh sách các tọa độ điểm trọng tâm sau mỗi vòng lặp(centroids_Hist)

- File code là file ******[exam.02.py][exam_02_py]******
- Chỉ chạy bài tập 2 ```python exam.02.py```
- Output trong folder ***[/output/exam.02/...][output_exam_02]***

### Hàm kmeans

```
def kmeans(X, k, maxiter, seed=None):
    """
    chỉ ra số lượng nhóm k cần phân loại 
    và số vòng lặp tối đa maxiter của thuật toán
    """

    # Chọn ngẫu nhiên k điểm dữ liệu làm centroids
    max = 0
    tol = 0.001
    labels = 75*colors
    centroids = {}
    for i in range(k):
        f = np.random.randint(0, len(X) - 1)
        centroids[i] = X[f]
    new_centroids = centroids.copy()

    for itr in range(maxiter):
        # ---------------
        # Bước gán vào phân nhóm
        # ---------------
        # Tính ma trận khoảng cách giữa mỗi điểm dữ liệu với các  centroids
        cluster_assignment = {}
        for i in range(k):
            cluster_assignment[i] = []

        # distance_matrix = # chỉ số hàng = chỉ số điểm dữ liệu; chỉ số cột = chỉ số của các centroids; giá trị = khoảng cách
        # gán mỗi điểm dữ liệu vào cùng nhóm với centroid gần nhất
        # cluster_assignment = # chỉ số mảng = chỉ số điểm dữ liệu; giá trị mảng = chỉ số của centroid gần nhất
        for index, xy in enumerate(X):
            distance_matrix = [np.linalg.norm(xy-centroids[centroid]) for centroid in centroids]
            cluster_ = distance_matrix.index(min(distance_matrix))
            c = cluster_ % len(colors)
            color = colors[c]
            labels[index] = color
            cluster_assignment[cluster_].append(xy)

        # Bước cập nhật:
        # chọn tất cả các điểm dữ liệu thuộc nhóm itr và tính
        # tọa độ trung bình của tất cả các điểm dữ liệu mới
        # tọa độ trung bình đó sẽ là vị trí mới của centroid của nhóm itr
        # new_centroids = ...
        for cluster_ in cluster_assignment:
            arr = cluster_assignment[cluster_]
            new_centroids[cluster_] = np.average(arr, axis=0)
            n = len(arr)
            if(n > max):
                max = n
            

        # Điều kiện dừng:
        # nếu toàn bộ vị trí của các centroids không thay đổi sau vòng lặp
        # => thoát vòng lặp
        isBreak = False
        for c in centroids:
            a = centroids[c]
            b = new_centroids[c]
            if np.sum((b-a)/a*100.0) > tol:
                isBreak = True
                break

        if isBreak:
            break

    centers_Hist = []
    new_centroids = np.array(list(new_centroids.values()), dtype=object)
    cluster_assignment = np.array(list(cluster_assignment.values()), dtype=object)


    print("max", max)
    for i in range(len(centroids)):
        arr = cluster_assignment[i]
        n = max - len(arr)
        if n <= 0:
            pass
        f = len(arr)-1
        itm = arr[f]
        for j in range(n):
            arr.append(itm)
        centers_Hist.append(np.array(arr))

    centers_Hist = np.array(centers_Hist)
    data = [labels, new_centroids, centers_Hist]

    return data
```

### Áp dụng thuật toán lên bộ dữ liệu

Giá trị điểm tọa độ ngẫu nhiên: xy

- Kết quả trong từng vòng lặp mang giá trị thế nào thì dùng giá trị trả về như thế ấy để dự đoán xy(dùng tập centroids trả về tại mỗi vòng lặp để dự đoán nhóm của xy trong mỗi vòng lặp)
- xy được phân nhóm dựa vào tập centroids đã hiệu chỉnh chứ không đọc lại toàn bộ data
- xy gần điểm centroid nào nhất thì đưa nó vào nhóm đó
- xy được đánh dấu dạng X trên biểu đồ với viền màu vàng và nền màu centroid tương ứng


```
def predict(data,labels,xy):
    model = KNeighborsClassifier(n_neighbors=1)
    model.fit(data,labels)
    predicted = model.predict([xy])
    return predicted

for i in range(1,3):
    # áp dụng thuật toán K-Means trên tập dữ liệu đầu vào
    labels, centers, centers_Hist = kmeans(X=data,k=Nb_cluster,maxiter=numIterations)
    print('Các điểm trung tâm thuật toán K-Means tìm được cho trường hợp {0}: \n'.format(i),  centers)
    x = np.random.uniform(-4, 4)
    y = np.random.uniform(-2, 10)
    xy = [x, y]
    class_ = predict(data=centers,labels=colors,xy=xy)
    print("Dự đoán {xy}".format(xy=xy), class_)

    # vẽ biểu đồ hiển thị kết quả của thuật toán K-Means
    plt.subplot(1,2,i)
    plt.title('Trường hợp {0}'.format(i))
    plot_kmeans(data, labels, centers, centers_Hist)
    plt.plot([x], [y], c = "gold", marker="X", markerfacecolor=class_[0])

plt.savefig("output/exam.02/chart/02. scatter.plot.predict.png")
```

### Input

<img alt="Phân tích dữ liệu" src="./output/exam.02/chart/01. scatter.plot.data.png" />

### Output

<img alt="loop kmeans" src="./output/exam.02/chart/02. scatter.plot.predict.png" />

# Bài tập 3
## Học sâu (Deep Learning) - Đề mở:
- Bài toán về thị giác máy tính: nhận diện phân loại đồ thời trang
- Đầu vào là 1 ảnh hoặc 1 tập ảnh
- Đầu ra là ảnh đó thuộc nhóm thời gian nào(quần, áo, giày dép, váy...)
- Mô hình học sâu: Tensorflow Sequential
- Data training, testing: tensorflow.keras.datasets.fashion_mnist
- Nguồn tham khảo [Google Codelabs][computervision]

- File code là file ******[exam.03.py][exam_03_py]******
- Chỉ chạy bài tập 3 ```python exam.03.py```
- Output in console

# Bài lý thuyết
### 4.1 Hãy nêu định nghĩa của vấn đề overfitting
Overfitting là một khái niệm đề cập đến tình trạng mô hình quá khớp so với dữ liệu huấn luyện tuy nhiên nó lại không dự đoán chính xác đối với các dữ liệu chưa nhìn thấy. Hay nói cách khác là mô hình không thể tổng quát hóa tốt cho dữ liệu mới, dẫn đến nó không thể phân loại hoặc dự đoán tốt.

### 4.2 Mô hình có độ lỗi thấp trên tập huấn luyện sẽ cho ra kết quả tốt nhất khi dự đoán trên tập kiểm tra đúng hay sai?
Một mô hình có độ lỗi thấp trên tập huấn luyện không có căn cứ nào cho rằng nó sẽ cho ra kết quả tốt với tập kiểm tra. Vì cơ bản là tập huấn luyện và tập kiểm tra là hai tập dữ liệu khác nhau. Trong trường hợp tập huấn luyện và tập kiểm tra là cùng một tập thì khi mô hình cho ra kết quả tốt lại không đáng tin cập(nó trở lại vấn đề ở mục 4.1).

### 4.3 Bảng dưới đây thể hiện 04 mô hình với các tham số khác nhau trên một tập dữ liệu nào đó. Mô hình nào khớp với dữ liệu nhất?

| Mô hình | Tham số (intercept, slope) | Residual Sum of Square (RSS) |
|---------|----------------------------|------------------------------|
| 1       |     (0, 1.4)               |     20.51                    |
| 2       |     (3.1, 1.4)             |     15.23                    |
| 3       |     (2.7, 1.9)             |     13.67                    |
| 4       |     (0, 2.3)               |     18.99                    |

Thông thường, giá trị RSS được cho rằng càng nhỏ thì mô hình hồi qui càng lí tưởng vì nó có nghĩa là có ít điểm ngoại lai hơn trong tập dữ liệu. Nói cách khác, tổng số dư bình phương càng thấp thì mô hình hồi qui càng tốt trong việc giải thích dữ liệu.

Từ điều trên cho thấy **mô hình số 3** là mô hình khớp với dữ liệu nhất

### 4.4 Mô hình đề xuất dựa trên mức độ phổ biến toàn cục có thể (có thể chọn nhiều đáp án):
-   Cá nhân hóa
-   Nắm bắt ngữ cảnh (Ví dụ: thời gian trong ngày)
-   Không điều nào ở trên đúng

Chọn: câu c không đáp án nào đúng


### 4.5 Mô hình đề xuất dựa trên phương pháp phân loại có thể (có thể chọn nhiều đáp án):
-   Cá nhân hóa
-   Nắm bắt ngữ cảnh (Ví dụ: thời gian trong ngày)
-   Không điều nào ở trên đúng

Chọn: câu a cá nhân hóa, câu b ngữ cảnh

### 4.6 Mô hình đề xuất sử dụng ma trận đồng xuất hiện (có thể chọn nhiều đáp án):
-   Cá nhân hóa
-   Nắm bắt ngữ cảnh (Ví dụ: thời gian trong ngày)
-   Không điều nào ở trên đúng

Chọn: câu c không đáp án nào đúng

### 4.7 Mô hình đề xuất sử dụng ma trận phân rã (có thể chọn nhiều đáp án):
-   Cá nhân hóa
-   Nắm bắt ngữ cảnh (Ví dụ: thời gian trong ngày)
-   Không điều nào ở trên đúng

Chọn: câu c không đáp án nào đúng

### 4.8 Chuẩn hóa ma trận đồng xuất hiện được sử dụng chủ yếu để trích xuất thông tin nào?
Chuẩn hóa ma trận đồng xuất hiện được sử dụng chủ yếu để trích xuất trọng số


### 4.9 Một cửa hàng có 3 khách hàng và 3 sản phẩm. Trong 02 bảng dưới đây là các vectơ đặc trưng đã trích lọc (extract) cho từng người dùng và sản phẩm. Dựa trên mô hình ước tính này, sản phẩm nào bạn sẽ giới thiệu cho người dùng thứ 2 trong bảng?

|     Người dùng |     Vectơ đặc trưng       |
|----------------|---------------------------|
|     1          |     (1.73, 0.01, 5.22)    |
|     2          |     (0.03, 4.41, 2.05)    |
|     3          |     (1.13, 0.89, 3.76)    |

|     Sản phẩm |     Vectơ đặc trưng       |
|--------------|---------------------------|
|     1        |     (3.29, 3.44, 3.67)    |
|     2        |     (0.82, 9.71, 3.88)    |
|     3        |     (8.34, 1.72, 0.02)    |

Chọn: giới thiệu sản phẩm số 1

<img alt="Biểu đồ" src="./output/ly-thuyet-09.png" />


### 4.10 + 4.11 Đối với các mặt hàng được yêu thích và hệ thống đề xuất hiển thị dưới đây, hãy tính recall và làm tròn đến 2 chữ số thập phân. (Như trong bài học, hình vuông màu xanh lá cây chỉ ra các mặt hàng được hệ thống đề xuất, hình vuông màu đỏ tươi là những mặt hàng được yêu thích. Các mục không được khuyến nghị được xám cho rõ ràng.

<img alt="Dữ liệu" src="./output/ly-thuyet-10-11.png" />

<img alt="Phân tích" src="./output/love-or-not.png" />

Recall		=  (true positive) / (true positive + false negative)	= 1 / (1 + 2) = 0.33

Precision	= (true positive)/ (true positive + false positive)		= 1 / (1 + 3) = 0.25

### 4.12 Dựa vào biểu đồ đường cong precision-recall trong hình ảnh bên dưới, hệ thống đề xuất nào nên được đưa vào sử dụng?

<img alt="Area Under The Curve" src="./output/ly-thuyet-12.png" />

Chọn: đường màu xanh nước biển **RecSys #1** vì nó có độ phủ AUC lớn nhất




[gitlab_ainc01]: <https://gitlab.com/lyhoangnam/ai-nc01>
[dowload_python]: <https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe>
[download_visual_studio_code]: <https://az764295.vo.msecnd.net/stable/5554b12acf27056905806867f251c859323ff7e9/VSCodeSetup-x64-1.64.0.exe>
[link_exam]: <https://colab.research.google.com/drive/1BdsaafDKvhB3n3iNxtxl1QTLa5AttNJG?usp=sharing#scrollTo=RQ5m6wUFjKuO>
[kaggle_practical]: <https://www.kaggle.com/faressayah/practical-introduction-to-10-regression-algorithm>
[download_csv]: </data/USA_Housing.csv>
[exam_01_py]: </exam.01.py>
[exam_02_py]: </exam.02.py>
[exam_03_py]: </exam.03.py>
[output_exam_01]: </output/exam.01>
[output_exam_02]: </output/exam.02>
[computervision]: <https://developers.google.com/codelabs/tensorflow-2-computervision#0>
